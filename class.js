"use strict";
exports.__esModule = true;
//class
var Employee = /** @class */ (function () {
    function Employee(id, name, address) {
        this.address = address;
        this.id = id;
        this.name = name;
    }
    Employee.prototype.login = function () {
        return { name: "name", id: 1, email: "" };
    };
    Employee.prototype.getNameWithAddress = function () {
        return "".concat(this.name, " stays at ").concat(this.address); // this is called a string literals or in python terms string formatting 
    };
    return Employee;
}());
var jhon = new Employee(1, "jhon", "route 66");
var getaddress = jhon.getNameWithAddress();
console.log(jhon);
console.log(getaddress);
