export interface User{
  name: string;
  age?: number; //? makes a interface property optional
  id: number;
  email?: string;
}

let user:User = {name:"a",id:1,email:""};

interface Employees extends User{
  salary?:string;
}
let employee: Employees = {name:"j",id:2,email:"",salary:"1000"};
export interface Login{
  login():User;
}
