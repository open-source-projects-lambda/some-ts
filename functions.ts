//generic functions
function getItems<Type>(items: Type[]): Type[]{
  return new Array<Type>().concat(items); // here type is a place holder it doesn't mean anything else;
}

let concatResult = getItems<number>([1,2,3,4,5,6,7]);
let concatString = getItems<string>(["a","b","c","d","e","f"]);
