"use strict";
let fname;
let lname;
fname = "Rajrup";
lname = "das";
let newname = fname + lname.toUpperCase();
console.log(newname);
let age;
age = 25;
//let dob="97";
//let result = parsInt(dob);
//boolean datatype
let isValid = false;
console.log(isValid);
//arrays
let empList;
empList = ["str", "str2"];
let depList;
depList = [1, 2, 3, 4, 5, 6];
let result = depList.filter((num) => num > 2);
let fresult = depList.find((num) => num === 2);
let sum = depList.reduce((acc, num) => acc + num);
console.log(fresult);
console.log(result);
console.log(sum);
let c = 2 /* Color.Blue */;
//swap numbers
let swapNumbs;
function swapNumbers(num1, num2) {
    return [num1, num2];
}
swapNumbs = swapNumbers(10, 20);
swapNumbs[0];
swapNumbs[1];
//any datatype
let department;
department = "11";
department = 10;
//function add(num1:number, num2:number):any{
//return num1 + num2;
//}
//let newsum = add(10,20);
//functions
function add(num1, num2, num3) {
    return num3 ? num1 + num2 + num3 : num1 + num2;
}
console.log(add(2, 3));
console.log(add(2, 3, 5));
const sub = (num1, num2) => num1 - num2;
console.log(sub(2, 3));
const mult = function (num1, num2) {
    return num1 + num2;
};
//rest parameter
function addi(num1, num2, ...num3) {
    return num1 + num2 + num3.reduce((a, b) => a + b, 0);
}
let numbers = [1, 2, 3, 4, 5, 6, 7, 8];
console.log(addi(2, 3, ...numbers));
