import { Login } from './interfaces';
import { User } from './interfaces';
//class
class Employee implements Login{
  id!:number;
  name!:string;
  address!:string;
  constructor(id:number,name:string,address:string){
    this.address=address;
    this.id=id;
    this.name=name;
  }
  login(): User{
    return {name:"name",id:1,email:""};
  }
  getNameWithAddress(): string{
    return `${this.name} stays at ${this.address}`; // this is called a string literals or in python terms string formatting 
  }
}
let jhon = new Employee(1,"jhon","route 66");
let getaddress = jhon.getNameWithAddress();
console.log(jhon);
console.log(getaddress);
