//generic functions
function getItems(items) {
    return new Array().concat(items); // here type is a place holder it doesn't mean anything else;
}
var concatResult = getItems([1, 2, 3, 4, 5, 6, 7]);
var concatString = getItems(["a", "b", "c", "d", "e", "f"]);
